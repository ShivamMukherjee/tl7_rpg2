﻿using TL7.Core;
using TL7.Attributes;
using UnityEngine;
using UnityEngine.AI;
using System;

namespace TL7.Movement
{
    [RequireComponent(typeof(ActionScheduler))]
    [RequireComponent(typeof(GameDevTV.Saving.SaveableEntity))]
    public class Mover : MonoBehaviour, IAction, GameDevTV.Saving.ISaveable
    {
        [SerializeField] bool kickAroundDeadBody = false;
        [SerializeField, Range(3f, 10f)] float maxSpeed = 5f;
        [SerializeField] float maxNavPathLength = 40f;
        Health health;
        Animator animator;
        NavMeshAgent navMeshAgent;
        ActionScheduler actionScheduler;


        // Using start will cause uninitialised references
        void Awake()
        {
            health = GetComponent<Health>();
            animator = GetComponent<Animator>();
            navMeshAgent = GetComponent<NavMeshAgent>();
            actionScheduler = GetComponent<ActionScheduler>();
        }

        void Update()
        {
            navMeshAgent.enabled = !health.IsDead || kickAroundDeadBody;
            UpdateAnimator();
        }

        private void UpdateAnimator()
        {
            float forwardSpeed = transform.InverseTransformDirection(navMeshAgent.velocity).z;
            animator.SetFloat(nameof(forwardSpeed), forwardSpeed);
        }

        public bool CanMoveTo(in Vector3 location)
        {
            var outPath = new NavMeshPath();
            bool pathCalculated = NavMesh.CalculatePath(
                transform.position,
                location,
                NavMesh.AllAreas,
                outPath
            );
            bool completed = (outPath.status == NavMeshPathStatus.PathComplete);
            Func<bool> IsPathShorterThanThreshold = () => (PathDistance(outPath) <= maxNavPathLength);
            return pathCalculated && completed && IsPathShorterThanThreshold();
        }

        private static float PathDistance(in NavMeshPath path)
        {
            float distance = 0f;
            for (var i = 1; i < path.corners.Length; ++i)
            {
                distance += Vector3.Distance(path.corners[i - 1], path.corners[i]);
            }
            return distance;
        }

        public void StartMoveAction(in Vector3 destination, in float speedFraction = 1f)
        {
            actionScheduler.StartAction(this);
            MoveTo(destination, speedFraction);
        }

        public void StartAction(params object[] args) =>
            StartMoveAction((Vector3)args[0], (args.Length == 2) ? (float)args[1] : 1f);

        public void MoveTo(in Vector3 destination, in float speedFraction)
        {
            if (navMeshAgent.isOnNavMesh)
            {
                navMeshAgent.destination = destination;
                navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
                navMeshAgent.isStopped = false;
            }
        }

        public void CancelAction() => navMeshAgent.isStopped = navMeshAgent.isOnNavMesh;

        object GameDevTV.Saving.ISaveable.CaptureState()
        {
            return new TL7.Saving.SerializedBasis(transform.position, transform.rotation);
        }

        void GameDevTV.Saving.ISaveable.RestoreState(object state)
        {
            var basis = (TL7.Saving.SerializedBasis)state;
            navMeshAgent.Warp(basis.Position);
            transform.rotation = basis.Rotation;
            actionScheduler.CancelCurrentAction();
        }
    }
}
