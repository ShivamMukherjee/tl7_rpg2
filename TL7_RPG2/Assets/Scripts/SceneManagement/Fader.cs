﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace TL7.SceneManagement
{
    public class Fader : MonoBehaviour
    {
        [SerializeField] float fadeDuration = .5f;
        private CanvasGroup canvasGroup;
        private Coroutine activeFadeEffect;

        void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void FadeOutImmediate()
        {
            canvasGroup.alpha = 1f;
        }

        private IEnumerator FadeCoroutineStep(float targetAlpha)
        {
            Assert.IsTrue((targetAlpha == 0f) || (targetAlpha == 1f));
            while (!Mathf.Approximately(canvasGroup.alpha, targetAlpha))
            {
                float deltaAlpha = Time.deltaTime / fadeDuration;
                canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, targetAlpha, deltaAlpha);
                yield return new WaitForEndOfFrame();
            }
        }

        public Coroutine Fade(float targetAlpha)
        {
            if (activeFadeEffect != null)
            {
                StopCoroutine(activeFadeEffect);
            }
            activeFadeEffect = StartCoroutine(FadeCoroutineStep(targetAlpha));
            return activeFadeEffect;
        }

        public Coroutine FadeOut()
        {
            return Fade(1f);
        }

        public Coroutine FadeIn()
        {
            return Fade(0f);
        }
    }
}
