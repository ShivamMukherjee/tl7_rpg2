﻿using System.Collections;
using TL7.Control;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace TL7.SceneManagement
{
    public class Portal : MonoBehaviour
    {
        [SerializeField, Range(-1, 100)] int sceneBuildIndexToTravelTo = -1;
        [SerializeField] string portalToSpawnAt = string.Empty;

        private Transform SpawnPoint => transform.GetChild(0);
        private GameObject Player => GameObject.FindWithTag("Player");

        void Start()
        {
            var collider = GetComponent<BoxCollider>();
            bool spawnPointInsidePortalBounds = collider.bounds.Contains(SpawnPoint.position);
            string messageIfNot = $"{name}: Spawn point must be outside the portal's bounds!";
            Assert.IsFalse(spawnPointInsidePortalBounds, messageIfNot);
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                StartCoroutine(Transit());
            }
        }

        private IEnumerator Transit()
        {
            if (sceneBuildIndexToTravelTo >= 0)
            {
                var fader = FindObjectOfType<Fader>();
                var savingWrapper = FindObjectOfType<SavingWrapper>();
                DontDestroyOnLoad(gameObject);
                Player.GetComponent<PlayerController>().enabled = false;
                savingWrapper.Save();

                // Run these two async
                yield return fader.FadeOut();
                yield return SceneManager.LoadSceneAsync(sceneBuildIndexToTravelTo);
                Player.GetComponent<PlayerController>().enabled = false;

                savingWrapper.Load();
                TeleportPlayerToMarkedPortal();
                savingWrapper.Save();

                // Call fade in but immediately all code next
                fader.FadeIn();
                Player.GetComponent<PlayerController>().enabled = true;
                Destroy(gameObject);
            }
        }

        private void TeleportPlayerToMarkedPortal()
        {
            var portal = GameObject.Find(portalToSpawnAt)?.GetComponent<Portal>();
            if (portal != null)
            {
                Player.GetComponent<NavMeshAgent>().Warp(portal.SpawnPoint.position);
                Player.transform.rotation = portal.SpawnPoint.rotation;
            }
        }
    }
}
