﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;

namespace TL7.Inventories
{
    [CreateAssetMenu(menuName = "TL7/Inventory/Drop Library")]
    public class DropLibrary : ScriptableObject
    {
        [SerializeField] DropConfig[] potentialDrops = null;
        [SerializeField, Range(0f, 1f)] float[] dropChance = null;
        [SerializeField] int[] minDrops = null;
        [SerializeField] int[] maxDrops = null;

        public IEnumerable<Dropped> GetRandomDrops(int level)
        {
            if (ShouldDrop(level))
            {
                for (int i = 0; i < GetRandomDropCount(level); ++i)
                {
                    yield return GetRandomDrop(level);
                }
            }
        }

        private bool ShouldDrop(int level)
        {
            return UnityEngine.Random.value < AtLevel(dropChance, level);
        }

        public int GetRandomDropCount(int level)
        {
            return UnityEngine.Random.Range(AtLevel(minDrops, level), AtLevel(maxDrops, level) + 1);
        }

        private Dropped GetRandomDrop(int level)
        {
            var drop = SelectRandomItem(level);
            return new Dropped
            {
                item = drop.item,
                count = drop.GetRandomCount(level)
            };
        }

        private DropConfig SelectRandomItem(int level)
        {
            float totalChance = FindTotalChance(level);
            float randomRoll = UnityEngine.Random.Range(0f, totalChance);
            float chance = 0f;
            foreach (DropConfig drop in potentialDrops)
            {
                chance += AtLevel(drop.relativeChance, level);
                if (chance > randomRoll)
                {
                    return drop;
                }
            }
            return null;
        }

        private float FindTotalChance(int level)
        {
            float sum = 0f;
            foreach (var drop in potentialDrops)
            {
                sum += AtLevel(drop.relativeChance, level);
            }
            return sum;
        }

        [Serializable]
        class DropConfig
        {
            public InventoryItem item = null;
            public float[] relativeChance = null;
            public int[] minCount = null;
            public int[] maxCount = null;

            public int GetRandomCount(int level)
            {
                if (!item.IsStackable())
                {
                    return 1;
                }
                int min = AtLevel(minCount, level);
                int max = AtLevel(maxCount, level);
                return UnityEngine.Random.Range(min, max + 1);
            }
        }

        static T AtLevel<T>(T[] values, int level)
        {
            if (values.Length == 0 || level < 0)
            {
                return default;
            }
            if (level >= values.Length)
            {
                return values[values.Length - 1];
            }
            return values[level];
        }

        public struct Dropped
        {
            public InventoryItem item;
            public int count;
        }
    }
}
