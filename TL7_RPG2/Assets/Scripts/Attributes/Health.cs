﻿using GameDevTV.Utils;
using TL7.Combat;
using TL7.Core;
using GameDevTV.Saving;
using TL7.Stats;
using UnityEngine;
using UnityEngine.Events;

namespace TL7.Attributes
{
    [RequireComponent(typeof(SaveableEntity))]
    public class Health : MonoBehaviour, ISaveable
    {
        [System.Serializable]
        public class TakeDamageEvent : UnityEvent<string> { }

        [SerializeField] float defaultHitPoints = 100f;
        [SerializeField] bool immortal = false;
        [SerializeField, Range(0f, 1f)] float hpRegeneratedOnLevelUpFraction = 1f;
        [SerializeField] TakeDamageEvent onDamaged = null;
        [SerializeField] UnityEvent onDeath = null;
        private LazyValue<float> lazyHP;
        private bool dead = false;
        private BaseStats stats;
        private new Collider collider;
        private Animator animator;
        private ActionScheduler actionScheduler;

        public float HitPoints
        {
            get => lazyHP.Value;
            private set
            {
                lazyHP.Value = Mathf.Clamp(value, 0, MaxHitPoints);
                DieOrResurrect();
            }
        }

        public float MaxHitPoints => stats.Of(StatType.Health);
        public float Fraction => HitPoints / MaxHitPoints;
        public bool IsDead => dead;


        void Awake()
        {
            stats = GetComponent<BaseStats>();
            collider = GetComponent<Collider>();
            animator = GetComponent<Animator>();
            actionScheduler = GetComponent<ActionScheduler>();
            lazyHP = new LazyValue<float>(initializer: () =>
                (stats != null) ? MaxHitPoints : defaultHitPoints);
        }

        void OnEnable() => stats.LevelledUp += RegenHPFraction;
        void OnDisable() => stats.LevelledUp -= RegenHPFraction;

        void Start()
        {
            lazyHP.ForceInit();
        }

        private void RegenHPFraction()
        {
            // Restore a fraction of the total health on level up
            HitPoints = Mathf.Max(HitPoints, MaxHitPoints * hpRegeneratedOnLevelUpFraction);
        }

        public void TakeDamage(in float damage, in Fighter instigator = null)
        {
            if (!dead && !immortal)
            {
                // Check if it's the killing blow [[ MONT'KA ]] and if you're not hurting yourself
                if ((HitPoints - damage <= 0) && (instigator != null) && (instigator != GetComponent<Fighter>()))
                {
                    onDeath.Invoke();
                    instigator?.GetComponent<Experience>()?.AwardXP(stats.Of(StatType.ExperienceReward));
                }
                // Actually apply the damage
                HitPoints -= damage;
                onDamaged.Invoke($"{damage}");
                print($"{gameObject} received {damage} damage from {instigator?.gameObject}!");
            }
        }

        public void Heal(in float amount)
        {
            if (!dead)
            {
                HitPoints += amount;
            }
        }

        private void DieOrResurrect()
        {
            if ((HitPoints <= 0) && !dead)
            {
                dead = true;
                collider.enabled = false;
                animator.SetTrigger("die");
                actionScheduler.CancelCurrentAction();
            }
            else if ((HitPoints > 0) && dead)
            {
                dead = false;
                collider.enabled = true;
                animator.SetTrigger("resurrect");
            }
        }

        object ISaveable.CaptureState()
        {
            return HitPoints;
        }

        void ISaveable.RestoreState(object state)
        {
            HitPoints = (float)state;
        }
    }
}
