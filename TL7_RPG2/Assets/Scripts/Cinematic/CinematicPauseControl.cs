﻿using TL7.Attributes;
using TL7.Control;
using TL7.Core;
using UnityEngine;
using UnityEngine.Playables;

namespace TL7.Cinematic
{
    public class CinematicPauseControl : MonoBehaviour
    {
        [SerializeField] Canvas[] canvasesToHide = null;
        private GameObject player;

        void Awake()
        {
            player = GameObject.FindWithTag("Player");
        }

        void OnEnable()
        {
            GetComponent<PlayableDirector>().played += DisableControl;
            GetComponent<PlayableDirector>().played += DisableUI;
            GetComponent<PlayableDirector>().stopped += EnableControl;
            GetComponent<PlayableDirector>().stopped += EnableUI;
        }

        void OnDisable()
        {
            GetComponent<PlayableDirector>().played -= DisableControl;
            GetComponent<PlayableDirector>().played -= DisableUI;
            GetComponent<PlayableDirector>().stopped -= EnableControl;
            GetComponent<PlayableDirector>().stopped -= EnableUI;
        }

        private void SwitchUI(in bool enabled)
        {
            for (int i = 0; i < canvasesToHide.Length; i++)
            {
                canvasesToHide[i].enabled = enabled;
            }
            var bots = FindObjectsOfType<AIController>();
            for (var i = 0; i < bots.Length; ++i)
            {
                bots[i].GetComponentInChildren<Canvas>().enabled = enabled;
            }
        }

        private void DisableUI(PlayableDirector _ = null)
        {
            SwitchUI(false);
        }

        private void EnableUI(PlayableDirector _ = null)
        {
            SwitchUI(true);
        }

        void DisableControl(PlayableDirector _ = null)
        {
            player.GetComponent<ActionScheduler>().CancelCurrentAction();
            player.GetComponent<PlayerController>().enabled = false;
        }

        void EnableControl(PlayableDirector _ = null) =>
            player.GetComponent<PlayerController>().enabled = true;
    }
}
