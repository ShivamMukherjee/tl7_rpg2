﻿using GameDevTV.Utils;
using TL7.Combat;
using TL7.Core;
using TL7.Movement;
using TL7.Attributes;
using UnityEngine;
using System;

namespace TL7.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] float chaseDistance = 5f;
        [SerializeField] float suspicionTime = 5f;
        [SerializeField] float aggroCooldownTime = 10f;
        [SerializeField, Range(0f, 1f)] float patrolSpeedFraction = .25f;
        [Header("Patrolling")]
        [SerializeField] PatrolCircuit patrolCircuit = null;
        [SerializeField] float waypointRadius = 2f;
        [SerializeField] float waypointDwellTime = 5f;
        [SerializeField] int firstWaypointIndex = 0;
        [SerializeField] float aggrevateMobDistance = 3f;
        private Mover mover;
        private Health health;
        private Fighter fighter;
        private LazyValue<Vector3> guardSpot;
        private GameObject player;
        private ActionScheduler actionScheduler;
        private float timeSincePlayerWasLastSpotted = Mathf.Infinity;
        private float timeDwelledAtWaypoint = Mathf.Infinity;
        private float timeSinceAggravated = Mathf.Infinity;
        private int currentWaypointIndex = 0;

        void Awake()
        {
            mover = GetComponent<Mover>();
            health = GetComponent<Health>();
            fighter = GetComponent<Fighter>();
            actionScheduler = GetComponent<ActionScheduler>();
            player = GameObject.FindWithTag("Player");
            guardSpot = new LazyValue<Vector3>(initializer: () => transform.position);
        }

        void Start()
        {
            guardSpot.ForceInit();
            currentWaypointIndex = firstWaypointIndex;
        }

        // Update is called once per frame
        void Update()
        {
            if (!health.IsDead)
            {
                if (IsAggravated())
                {
                    AttackPlayer();
                }
                else if (timeSincePlayerWasLastSpotted <= suspicionTime)
                {
                    Suspect();
                }
                else
                {
                    Patrol();
                }
                UpdateTimers();
            }
        }

        private void UpdateTimers()
        {
            timeSincePlayerWasLastSpotted += Time.deltaTime;
            timeDwelledAtWaypoint += Time.deltaTime;
            timeSinceAggravated += Time.deltaTime;
        }

        private void AttackPlayer()
        {
            timeSincePlayerWasLastSpotted = 0f;
            timeDwelledAtWaypoint = Mathf.Infinity;
            fighter.StartAttackAction(player);
            AggravateMob();
        }

        private void AggravateMob()
        {
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, aggrevateMobDistance, Vector3.up, 0f);
            for (var i = 0; i < hits.Length; ++i)
            {
                hits[i].transform.GetComponent<AIController>()?.Aggravate();
            }
        }

        public void Aggravate()
        {
            timeSinceAggravated = 0f;
        }

        private bool IsAggravated()
        {
            float distanceFromPlayer = Vector3.Distance(player.transform.position, transform.position);
            bool playerIsWithinRange = (distanceFromPlayer < chaseDistance);
            bool aggravated = (timeSinceAggravated < aggroCooldownTime);
            return (playerIsWithinRange || aggravated) && fighter.CanTarget(player);
        }

        private void Patrol()
        {
            Vector3 nextSpot = guardSpot.Value;
            if (patrolCircuit != null)
            {
                if (IsAlreadyAtWaypoint())
                {
                    timeDwelledAtWaypoint = 0f;
                    CycleNextWaypoint();
                }
                nextSpot = patrolCircuit[currentWaypointIndex];
            }
            if (timeDwelledAtWaypoint >= waypointDwellTime)
            {
                mover.StartAction(nextSpot, patrolSpeedFraction);
                timeDwelledAtWaypoint = Mathf.Infinity;
            }
        }

        private bool IsAlreadyAtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, patrolCircuit[currentWaypointIndex]);
            return distanceToWaypoint < waypointRadius;
        }

        private void CycleNextWaypoint() => patrolCircuit.IncrementWaypointIndex(ref currentWaypointIndex);

        private void Suspect()
        {
            // Could be richer
            actionScheduler.CancelCurrentAction();
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, waypointRadius);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, Mathf.Clamp01(1 - timeSincePlayerWasLastSpotted / suspicionTime));
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(transform.position, Mathf.Clamp01(1 - timeDwelledAtWaypoint / waypointDwellTime));
        }
    }
}
