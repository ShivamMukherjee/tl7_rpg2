﻿using UnityEngine;

namespace TL7.Control
{
    public abstract class ControlSource : ScriptableObject
    {
        public abstract Ray ScreenPositionSignal { get; }

        public abstract bool LeftClickSignal { get; }
    }
}
