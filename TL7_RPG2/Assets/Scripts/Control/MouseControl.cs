﻿using UnityEngine;

namespace TL7.Control
{

    [CreateAssetMenu(fileName = "Mouse Control Source", menuName = "TL7/Control/New Mouse Control", order = 0)]
    public class MouseControl : ControlSource
    {
        public override Ray ScreenPositionSignal => Camera.main.ScreenPointToRay(Input.mousePosition);

        public override bool LeftClickSignal => Input.GetMouseButton(0);
    }
}
