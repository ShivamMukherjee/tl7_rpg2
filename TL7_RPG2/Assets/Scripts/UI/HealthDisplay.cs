﻿using TL7.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace TL7.UI
{
    public class HealthDisplay : MonoBehaviour, IHUDText
    {
        [SerializeField] bool showAsPercentage = true;
        Health health;
        Text hpPercentage;
        string prefix;

        string IHUDText.Prefix => prefix;
        Text IHUDText.Text => hpPercentage;

        void Awake()
        {
            health = GameObject.FindWithTag("Player").GetComponent<Health>();
            hpPercentage = GetComponent<Text>();
            prefix = hpPercentage?.text;
        }

        void Update()
        {
            hpPercentage.text = prefix + (showAsPercentage?
                $"{(health.Fraction * 100) : 0.0}%" :
                $"{health.HitPoints:0.0} / {health.MaxHitPoints:0.0}");
        }
    }
}
