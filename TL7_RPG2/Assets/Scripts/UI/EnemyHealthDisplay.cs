﻿using TL7.Combat;
using TL7.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace TL7.UI
{
    public class EnemyHealthDisplay : MonoBehaviour, IHUDText
    {
        [SerializeField] bool showAsPercentage = true;
        Health targetHealth;
        Text hpPercentage;
        string prefix;

        string IHUDText.Prefix => prefix;
        Text IHUDText.Text => hpPercentage;

        void Awake()
        {
            hpPercentage = GetComponent<Text>();
            // Must contain a format string. Otherwise fails silently.
            prefix = hpPercentage?.text;
        }

        void Update()
        {
            targetHealth = GameObject.FindWithTag("Player").GetComponent<Fighter>().Target;
            string healthDisplayed = showAsPercentage ? 
                $"{(targetHealth?.Fraction * 100) : 0.0}%" :
                $"{targetHealth?.HitPoints:0.0} / {targetHealth?.MaxHitPoints : 0.0}";
            hpPercentage.text = prefix + ((targetHealth != null) ? healthDisplayed : "None");
        }
    }
}
