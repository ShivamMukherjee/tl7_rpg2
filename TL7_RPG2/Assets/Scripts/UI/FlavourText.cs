﻿using UnityEngine;
using UnityEngine.UI;

namespace TL7.UI
{
    public class FlavourText : MonoBehaviour
    {
        void DestroyText() => Destroy(gameObject);

        public string Text { set => GetComponentInChildren<Text>().text = value; }
    }
}
