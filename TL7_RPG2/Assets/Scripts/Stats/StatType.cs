namespace TL7.Stats
{
    public enum StatType
    {
        Health,
        ExperienceReward,
        ExperienceToLevelUp,
        Damage
    }
}
