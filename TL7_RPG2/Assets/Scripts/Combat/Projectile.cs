﻿using TL7.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace TL7.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] float speed = 10;
        [Tooltip("Whether the projectile follows the target or not.")]
        [SerializeField] bool homing = false;
        [SerializeField] float lifetimeAfterHit = 5f;
        [Tooltip("Only used if homing is set.")]
        [SerializeField] float homingLifetime = 2f;
        [SerializeField] ParticleSystem impactParticles = null;
        [SerializeField] UnityEvent onHit = null;
        private Health target = null;
        private Fighter instigator;
        private new ParticleSystem particleSystem = null;
        private float damage;
        private bool hitSomething = false;

        private Vector3 AimLocation => target.GetComponent<Collider>().bounds.center;
        private bool IsLivingTarget => target != null && !target.IsDead;

        public void SetParameters(in Fighter instigator, in Health target, in float damage)
        {
            this.instigator = instigator;
            this.target = target;
            this.damage = damage;
        }

        void Awake()
        {
            particleSystem = gameObject.GetComponentInChildren<ParticleSystem>();
        }

        void Start()
        {
            if (target != null)
            {
                transform.LookAt(AimLocation);
            }

            if (homing && IsLivingTarget)
            {
                Destroy(gameObject, homingLifetime);
            }
        }

        void Update()
        {
            if (homing && IsLivingTarget)
            {
                // Reorients the projectile whenever the target moves
                transform.LookAt(AimLocation);
            }

            if (!hitSomething)
            {
                transform.Translate(speed * Vector3.forward * Time.deltaTime);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            // Prevent any effects if the object has a special tag (or two)
            if (other.CompareTag("Cinematics"))
            {
                return;
            }

            onHit.Invoke();

            if (IsLivingTarget)
            {
                // This yields interesting behaviour: friendly fire!
                other.GetComponent<Health>()?.TakeDamage(damage, instigator);
            }

            if (homing)
            {
                // Keeping collision enabled causes repeated damage
                GetComponent<Collider>().enabled = false;
                GetComponentInChildren<TrailRenderer>().enabled = false;
            }

            StickToTargetTillDestroyed(other);
            SpawnImpactFeedback();
        }

        private void StickToTargetTillDestroyed(Collider other)
        {
            transform.parent = other.transform;

            hitSomething = true;
            if (particleSystem != null)
            {
                var emission = particleSystem.emission;
                emission.enabled = false;
            }

            Destroy(gameObject, lifetimeAfterHit);
        }

        private void SpawnImpactFeedback()
        {
            if (impactParticles != null)
            {
                Instantiate(impactParticles, transform.position, Quaternion.identity);
            }
        }
    }
}
