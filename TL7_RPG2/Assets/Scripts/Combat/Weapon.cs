﻿using UnityEngine;
using UnityEngine.Events;

namespace TL7.Combat
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField] UnityEvent onHit = null;

        public void OnHit()
        {
            onHit.Invoke();
        }
    }
}
