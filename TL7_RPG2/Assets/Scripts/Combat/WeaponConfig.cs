﻿using System.Collections.Generic;
using GameDevTV.Inventories;
using TL7.Attributes;
using TL7.Stats;
using TL7.UI;
using UnityEngine;

namespace TL7.Combat
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "TL7/Combat/New Weapon Config", order = 1)]
    public class WeaponConfig : EquipableItem, IModifierProvider
    {
        [SerializeField, Range(0f, 10f)] float timeBetweenAttacks = 1f;
        [SerializeField, Range(0f, 20f)] float range = 2f;
        [SerializeField] float baseDamage = 5f;
        [SerializeField, Range(0f, 1f)] float multiplierBonus = .1f;
        [SerializeField] bool leftHanded = false;
        [SerializeField] Weapon prefab = null;
        [SerializeField] CursorType cursorType = CursorType.Sword;
        [SerializeField] AnimatorOverrideController animatorOverride = null;
        [SerializeField] Projectile projectilePrefab = null;
        private Fighter owner;

        public float Range => range;
        public float Damage => baseDamage;
        public float MultiplierBonus => multiplierBonus;
        public string PrefabName => (prefab != null) ? prefab.name : string.Empty;
        public bool IsLeftHanded => leftHanded;
        public bool HasProjectiles => (projectilePrefab != null);
        public CursorType CursorType => cursorType;
        public float TimeBetweenAttacks => timeBetweenAttacks;
        public AnimatorOverrideController AnimatorOverride => animatorOverride;

        public Weapon Spawn(in Fighter owner, in Animator animator)
        {
            Weapon weapon = null;

            if (prefab != null)
            {
                weapon = Instantiate(prefab, owner.Hand);
                prefab.GetComponentInChildren<ParticleSystem>()?.Play();
            }

            if (animator != null)
            {
                if (animatorOverride != null)
                {
                    animator.runtimeAnimatorController = animatorOverride;
                }
                // Check if the runtime controller is already set and is more specialized 
                else if (animator.runtimeAnimatorController is AnimatorOverrideController animatorOverride)
                {
                    // Reset animator to the default one (stored in AnimatorOverrideControllers)
                    animator.runtimeAnimatorController = animatorOverride.runtimeAnimatorController;
                }
            }

            return weapon;
        }

        public bool TryLaunchProjectile(in Fighter owner, in Health target, in float damage)
        {
            if (HasProjectiles)
            {
                var projectile = Instantiate(projectilePrefab, owner.Hand.position, Quaternion.identity);
                projectile.SetParameters(instigator: owner, target, damage);
                return true;
            }
            return false;
        }

        IEnumerable<float> IModifierProvider.AdditivesFor(StatType statType)
        {
            if (statType == StatType.Damage)
            {
                yield return Damage;
            }
        }

        IEnumerable<float> IModifierProvider.MultipliersFor(StatType statType)
        {
            if (statType == StatType.Damage)
            {
                yield return MultiplierBonus;
            }
        }
    }
}
